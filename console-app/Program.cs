﻿using System;
using Library;

namespace console_app {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine ($"The answer is {new Thing().Get(199, 23)}");
            Console.WriteLine ("there is something that i want to know");
            bool flag = true;
            for (int i = 0; i < 10; i++) {
                if (i == 5) flag = false;
                else flag = true;
                if (flag)
                    Console.WriteLine ($"In loop i = {i}");
            }
            int cnt = 0;
            while (flag) {
                if (cnt == 5) flag = false;
                string s = "IamNew";
                Console.WriteLine (s);
                cnt++;
            }
        }
    }
}